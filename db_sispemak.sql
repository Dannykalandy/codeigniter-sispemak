-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 02, 2020 at 06:43 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sispemak`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` int(11) NOT NULL,
  `category` int(1) NOT NULL,
  `thumbnail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `name`, `description`, `price`, `category`, `thumbnail`) VALUES
(10, 'Nasi Timbel', 'Timbel', 15000, 1, 'menu-makanan5.jpg'),
(11, 'Nasi Tutug Oncom', 'Oncom', 20000, 1, 'menu-makanan6.jpg'),
(12, 'Karedok', 'Sayuran', 10000, 1, 'menu-makanan7.jpg'),
(13, 'Es Cendol', 'Cendol', 15000, 2, 'menu-minuman4.jpg'),
(14, 'Es Goyobod', 'Goyobod', 15000, 2, 'menu-minuman5.jpg'),
(15, 'Es Oyen', 'Oyen', 20000, 2, 'menu-minuman6.jpg');

-- --------------------------------------------------------

--
-- Stand-in structure for view `menu_view`
-- (See below for the actual view)
--
CREATE TABLE `menu_view` (
`menu_id` int(11)
,`name` varchar(255)
,`description` text
,`price` int(11)
,`category` int(1)
,`category_label` varchar(7)
,`thumbnail` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `option`
--

CREATE TABLE `option` (
  `option_id` int(11) NOT NULL,
  `key` varchar(100) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `option`
--

INSERT INTO `option` (`option_id`, `key`, `value`) VALUES
(1, 'app_name', 'SISPEMAK'),
(2, 'greeting', 'Welcome'),
(3, 'app_description', 'Siap melayani anda!!!'),
(4, 'banner', 'banner1.png'),
(5, 'nama_penerima', 'Danny Kalandy'),
(6, 'no_rek', '123456789'),
(7, 'bank', 'LinkAja');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL,
  `reservasi_id` int(11) NOT NULL,
  `no_account` varchar(20) NOT NULL,
  `name_account` varchar(100) NOT NULL,
  `bank_account` varchar(10) NOT NULL,
  `proof` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `place`
--

CREATE TABLE `place` (
  `place_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `place`
--

INSERT INTO `place` (`place_id`, `name`, `thumbnail`) VALUES
(4, 'Ruangan AC', 'denah.png'),
(5, 'Ruangan Smoking', 'denah1.png');

-- --------------------------------------------------------

--
-- Table structure for table `place_detail`
--

CREATE TABLE `place_detail` (
  `place_detail_id` int(11) NOT NULL,
  `place_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `place_detail`
--

INSERT INTO `place_detail` (`place_detail_id`, `place_id`, `name`) VALUES
(18, 4, 'Meja No. 01'),
(19, 4, 'Meja No. 02'),
(20, 4, 'Meja No. 03'),
(21, 4, 'Meja No. 04'),
(22, 4, 'Meja No. 05'),
(23, 4, 'Meja No. 06'),
(24, 5, 'Meja No. 07'),
(25, 5, 'Meja No. 08'),
(26, 5, 'Meja No. 09'),
(27, 5, 'Meja No. 10'),
(28, 5, 'Meja No. 11'),
(29, 5, 'Meja No. 12');

-- --------------------------------------------------------

--
-- Stand-in structure for view `place_detail_view`
-- (See below for the actual view)
--
CREATE TABLE `place_detail_view` (
`place_id` int(11)
,`name` varchar(255)
,`thumbnail` varchar(255)
,`place_detail_id` int(11)
,`table_name` varchar(100)
,`reservasi_place_id` int(11)
,`user_id` int(11)
,`reservasi_id` int(11)
,`date` datetime
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `place_view`
-- (See below for the actual view)
--
CREATE TABLE `place_view` (
`place_id` int(11)
,`name` varchar(255)
,`thumbnail` varchar(255)
,`place_detail_id` int(11)
,`table_name` varchar(100)
);

-- --------------------------------------------------------

--
-- Table structure for table `reservasi`
--

CREATE TABLE `reservasi` (
  `reservasi_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservasi`
--

INSERT INTO `reservasi` (`reservasi_id`, `user_id`, `date`, `status`, `created_at`, `updated_at`) VALUES
(10, 16, '2020-07-02 23:00:00', 6, '2020-07-02 23:39:41', '2020-07-02 23:40:40');

-- --------------------------------------------------------

--
-- Table structure for table `reservasi_menu`
--

CREATE TABLE `reservasi_menu` (
  `reservasi_menu_id` int(11) NOT NULL,
  `reservasi_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservasi_menu`
--

INSERT INTO `reservasi_menu` (`reservasi_menu_id`, `reservasi_id`, `menu_id`, `sum`) VALUES
(25, 10, 11, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `reservasi_menu_view`
-- (See below for the actual view)
--
CREATE TABLE `reservasi_menu_view` (
`reservasi_menu_id` int(11)
,`reservasi_id` int(11)
,`user_id` int(11)
,`menu_id` int(11)
,`name` varchar(255)
,`price` int(11)
,`thumbnail` varchar(255)
,`sum` int(11)
,`total` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `reservasi_place`
--

CREATE TABLE `reservasi_place` (
  `reservasi_place_id` int(11) NOT NULL,
  `reservasi_id` int(11) NOT NULL,
  `place_detail_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservasi_place`
--

INSERT INTO `reservasi_place` (`reservasi_place_id`, `reservasi_id`, `place_detail_id`) VALUES
(21, 10, 18);

-- --------------------------------------------------------

--
-- Stand-in structure for view `reservasi_view`
-- (See below for the actual view)
--
CREATE TABLE `reservasi_view` (
`reservasi_id` int(11)
,`user_id` int(11)
,`name` varchar(255)
,`email` varchar(255)
,`menu` mediumtext
,`detail_table` mediumtext
,`jumlah_minuman` bigint(20) unsigned
,`jumlah_makanan` bigint(20) unsigned
,`can_add_table` decimal(16,0)
,`table` bigint(21)
,`date` datetime
,`total` decimal(42,0)
,`status` int(1)
,`created_at` datetime
,`updated_at` datetime
);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  `capability` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `email`, `password`, `name`, `capability`, `created_at`, `updated_at`) VALUES
(1, 'admin@sispemak.com', '933c33f8cb9d291c155eb07c6b3de9c8afac8b1f', 'Danny Kalandy', 2, '2017-11-29 18:44:47', '2020-07-02 21:44:03'),
(15, 'kasir@sispemak.com', 'b7e243f460150fe38c7973968ce3ea1f7c49eeb6', 'Kasir', 2, '2020-07-02 21:47:58', '2020-07-02 21:47:58'),
(16, 'dannykalandy@yahoo.co.id', '0bb87aa3694d443b4d6ed27160b6257c5c44a001', 'Danny Kalandy', 1, '2020-07-02 23:30:20', '2020-07-02 23:30:20');

-- --------------------------------------------------------

--
-- Structure for view `menu_view`
--
DROP TABLE IF EXISTS `menu_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `menu_view`  AS  select `menu`.`menu_id` AS `menu_id`,`menu`.`name` AS `name`,`menu`.`description` AS `description`,`menu`.`price` AS `price`,`menu`.`category` AS `category`,if(`menu`.`category` = 1,'makanan','minuman') AS `category_label`,`menu`.`thumbnail` AS `thumbnail` from `menu` ;

-- --------------------------------------------------------

--
-- Structure for view `place_detail_view`
--
DROP TABLE IF EXISTS `place_detail_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `place_detail_view`  AS  select `place_view`.`place_id` AS `place_id`,`place_view`.`name` AS `name`,`place_view`.`thumbnail` AS `thumbnail`,`place_view`.`place_detail_id` AS `place_detail_id`,`place_view`.`table_name` AS `table_name`,`reservasi_place`.`reservasi_place_id` AS `reservasi_place_id`,`reservasi`.`user_id` AS `user_id`,`reservasi`.`reservasi_id` AS `reservasi_id`,`reservasi`.`date` AS `date` from ((`place_view` join `reservasi_place` on(`place_view`.`place_detail_id` = `reservasi_place`.`place_detail_id`)) join `reservasi` on(`reservasi`.`reservasi_id` = `reservasi_place`.`reservasi_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `place_view`
--
DROP TABLE IF EXISTS `place_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `place_view`  AS  select `place`.`place_id` AS `place_id`,`place`.`name` AS `name`,`place`.`thumbnail` AS `thumbnail`,`place_detail`.`place_detail_id` AS `place_detail_id`,`place_detail`.`name` AS `table_name` from (`place` left join `place_detail` on(`place_detail`.`place_id` = `place`.`place_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `reservasi_menu_view`
--
DROP TABLE IF EXISTS `reservasi_menu_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `reservasi_menu_view`  AS  select `reservasi_menu`.`reservasi_menu_id` AS `reservasi_menu_id`,`reservasi_menu`.`reservasi_id` AS `reservasi_id`,`reservasi`.`user_id` AS `user_id`,`reservasi_menu`.`menu_id` AS `menu_id`,`menu`.`name` AS `name`,`menu`.`price` AS `price`,`menu`.`thumbnail` AS `thumbnail`,`reservasi_menu`.`sum` AS `sum`,`reservasi_menu`.`sum` * `menu`.`price` AS `total` from ((`reservasi_menu` join `reservasi` on(`reservasi`.`reservasi_id` = `reservasi_menu`.`reservasi_id`)) join `menu` on(`menu`.`menu_id` = `reservasi_menu`.`menu_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `reservasi_view`
--
DROP TABLE IF EXISTS `reservasi_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `reservasi_view`  AS  select `reservasi`.`reservasi_id` AS `reservasi_id`,`reservasi`.`user_id` AS `user_id`,`user`.`name` AS `name`,`user`.`email` AS `email`,ifnull(concat('[',group_concat(distinct concat('[',`menu`.`category`,',',`reservasi_menu`.`sum`,',"',`menu`.`name`,'"]') separator ','),']'),'[]') AS `menu`,ifnull(concat('[',group_concat(distinct concat('["',`place_detail`.`name`,'","',`place`.`name`,'"]') separator ','),']'),'[]') AS `detail_table`,cast(ifnull(sum(if(`menu`.`category` = 2,1 * `reservasi_menu`.`sum`,0)) / count(distinct `reservasi_place`.`place_detail_id`),sum(if(`menu`.`category` = 2,1 * `reservasi_menu`.`sum`,0))) as unsigned) AS `jumlah_minuman`,cast(ifnull(sum(if(`menu`.`category` = 1,1 * `reservasi_menu`.`sum`,0)) / count(distinct `reservasi_place`.`place_detail_id`),sum(if(`menu`.`category` = 1,1 * `reservasi_menu`.`sum`,0))) as unsigned) AS `jumlah_makanan`,ifnull(ceiling(sum(if(`menu`.`category` = 1,1 * `reservasi_menu`.`sum`,0) / 2) / count(distinct `reservasi_place`.`place_detail_id`)),ceiling(sum(if(`menu`.`category` = 1,1 * `reservasi_menu`.`sum`,0) / 2))) AS `can_add_table`,count(distinct `reservasi_place`.`place_detail_id`) AS `table`,`reservasi`.`date` AS `date`,sum(`menu`.`price` * `reservasi_menu`.`sum`) AS `total`,`reservasi`.`status` AS `status`,`reservasi`.`created_at` AS `created_at`,`reservasi`.`updated_at` AS `updated_at` from ((((((`reservasi` join `user` on(`reservasi`.`user_id` = `user`.`user_id`)) join `reservasi_menu` on(`reservasi`.`reservasi_id` = `reservasi_menu`.`reservasi_id`)) join `menu` on(`menu`.`menu_id` = `reservasi_menu`.`menu_id`)) left join `reservasi_place` on(`reservasi_place`.`reservasi_id` = `reservasi`.`reservasi_id`)) left join `place_detail` on(`place_detail`.`place_detail_id` = `reservasi_place`.`place_detail_id`)) left join `place` on(`place`.`place_id` = `place_detail`.`place_id`)) group by `reservasi`.`reservasi_id` order by `reservasi`.`reservasi_id` desc ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `option`
--
ALTER TABLE `option`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `reservasi_id` (`reservasi_id`);

--
-- Indexes for table `place`
--
ALTER TABLE `place`
  ADD PRIMARY KEY (`place_id`);

--
-- Indexes for table `place_detail`
--
ALTER TABLE `place_detail`
  ADD PRIMARY KEY (`place_detail_id`),
  ADD KEY `place_id` (`place_id`);

--
-- Indexes for table `reservasi`
--
ALTER TABLE `reservasi`
  ADD PRIMARY KEY (`reservasi_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `reservasi_menu`
--
ALTER TABLE `reservasi_menu`
  ADD PRIMARY KEY (`reservasi_menu_id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `reservasi_id` (`reservasi_id`);

--
-- Indexes for table `reservasi_place`
--
ALTER TABLE `reservasi_place`
  ADD PRIMARY KEY (`reservasi_place_id`),
  ADD KEY `reservasi_id` (`reservasi_id`),
  ADD KEY `place_detail_id` (`place_detail_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `option`
--
ALTER TABLE `option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `place`
--
ALTER TABLE `place`
  MODIFY `place_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `place_detail`
--
ALTER TABLE `place_detail`
  MODIFY `place_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `reservasi`
--
ALTER TABLE `reservasi`
  MODIFY `reservasi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `reservasi_menu`
--
ALTER TABLE `reservasi_menu`
  MODIFY `reservasi_menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `reservasi_place`
--
ALTER TABLE `reservasi_place`
  MODIFY `reservasi_place_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`reservasi_id`) REFERENCES `reservasi` (`reservasi_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `place_detail`
--
ALTER TABLE `place_detail`
  ADD CONSTRAINT `place_detail_ibfk_1` FOREIGN KEY (`place_id`) REFERENCES `place` (`place_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reservasi`
--
ALTER TABLE `reservasi`
  ADD CONSTRAINT `reservasi_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reservasi_menu`
--
ALTER TABLE `reservasi_menu`
  ADD CONSTRAINT `reservasi_menu_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservasi_menu_ibfk_2` FOREIGN KEY (`reservasi_id`) REFERENCES `reservasi` (`reservasi_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reservasi_place`
--
ALTER TABLE `reservasi_place`
  ADD CONSTRAINT `reservasi_place_ibfk_1` FOREIGN KEY (`reservasi_id`) REFERENCES `reservasi` (`reservasi_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservasi_place_ibfk_2` FOREIGN KEY (`place_detail_id`) REFERENCES `place_detail` (`place_detail_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
